# BPiM2Berry-Tools 0.5-beta

The BPiM2Berry-Tools are a toolset inspired by the popular "raspi-config", for the most important settings of the BananaPi M2 Berry. It
may work even on other BananaPi-boards & SBCs.

# Use at your own risk! The installation of "Nextcloud" is still under development, as well the part of getting WiFi options to work which is not yet implemented.

For installation:

git clone https://li0nic@gitlab.com/li0nic/BPiM2Berry-tools.git

cd BPiM2Berry-tools && chmod 777 install-bpi-tools.sh 

./install-bpi-tools.sh

cd

Now you can run the tools by simply using "bpi-config" !

For uninstallation simply type:

./deinstall-bpi-tools.sh
