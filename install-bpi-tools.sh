#!/bin/sh
#
echo
echo "   \033[36m Autoinstaller für M2Berry-tools\033[0m"
echo
echo "   \033[36m Author:     @li0nic\033[0m"
echo "   \033[36m Version:         0.5-beta\033[0m"
echo
echo "   \033[32m Are you sure you want to install 'bpi-config'?\033[0m"
echo "   \033[32m Your answer, n/y:\033[0m"
read answer
#echo Installation aborted
echo     Your answer was: $answer
# if [ "$answer" = "y" ]
if [ "$answer" != "n" ]
 then chmod +x bpi-config.sh resize.sh resizea.sh nextcloud.sh &&
sleep 1
cp deinstall-bpi-tools.sh /root/ &&
sleep 1
cp bpi-config.sh /usr/bin/bpi-config &&
sleep 1
# cp omv-install-3.x.sh /usr/bin/omv-install-3.x.sh &&
# sleep 1
# cp omv-install-2.x.sh /usr/bin/omv-install-2.x.sh &&
# sleep 1
cp resize.sh /usr/bin/resize
sleep 1
cp resizea.sh /usr/bin/resizea
sleep 1
cp nextcloud.sh /usr/bin/nextcloud
sleep 1
cd &&
sleep 1
chmod +x deinstall-bpi-tools.sh &&
sleep 1
cd &&
bpi-config
echo
echo
echo "   \033[32m That's all M2Berry-Tools\033[0m"
else echo "   \033[31m Installation aborted\033[0m"
fi
