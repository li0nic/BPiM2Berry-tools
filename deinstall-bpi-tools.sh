#!/bin/sh
#
echo
echo "   \033[36m Uninstaller for BPiM2Berry-Tools\033[0m"
echo
echo "   \033[36m Author:     SuBLiner\033[0m"
echo "   \033[36m Version:         1.0\033[0m"
echo
echo "   \033[32m Are you sure you want to uninstall the toolset?\033[0m"
echo "   \033[32m Please select, n/y:\033[0m"
read answer
#echo The uninstallation was canceled
echo     Your selection: $answer
# if [ "$answer" = "y" ]
if [ "$answer" != "n" ]
 then rm -r BPi2MBerry-tools && 
 rm -r deinstall-bpi-tools.sh &&
 cd /usr/bin/ && 
 rm -r bpi-config resize resizea nextcloud
echo
echo
echo "   \033[32m The toolset was succesfully uninstalled!\033[0m"
else echo "   \033[31m The uninstallation was canceled!\033[0m"
fi
