#!/bin/bash 

echo
echo -e "\033[36m Nextcloud\033[0m"
echo
echo -e "\033[36m Author:     SuBLiNeR\033[0m"
echo -e "\033[36m Version:         0.1\033[0m"
echo
echo -e "\033[32m Are you sure you want install 'Nextcloud' ? \033[0m"
echo -e "\033[32m Your answer, n/y:\033[0m"
read answer
#echo Das installieren wurde abgebrochen
echo  Your anwer: $answer
# if [ "$answer" = "y" ]
if [ "$answer" != "n" ]
  then apt-get update &&
sleep 1
apt-get --yes --force-yes --allow-unauthenticated install apache2 php5 php5-gd sqlite php5-sqlite php5-curl &&
sleep 1
service apache2 restart &&
sleep 1
wget https://download.nextcloud.com/server/releases/latest.zip &&
sleep 1
mv latest.zip /var/www/html &&
sleep 1
cd /var/www/html &&
sleep 1
unzip -q latest.zip &&
sleep 1
rm latest.zip &&
sleep 1
mkdir -p /var/www/html/nextcloud/data &&
sleep 1
chown www-data:www-data /var/www/html/nextcloud/data &&
sleep 1
chmod 750 /var/www/html/nextcloud/data &&
sleep 1
cd /var/www/html/nextcloud &&
sleep 1
chown www-data:www-data config apps &&
sleep 1
init 6
echo
echo
echo -e "\033[32m That's all, Nextcloud successfully installed! \033[0m"
else echo -e "\033[31m Installation failed \033[0m"
fi




